$(document).ready(function() {
    $(".menu-wrapper").find("a").click(function(e) {
        e.preventDefault();
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top
        });
    });

    $(window).scroll(function() {
        if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
            $('#return-to-top').fadeIn(200);    // Fade in the arrow
        } else {
            $('#return-to-top').fadeOut(200);   // Else fade out the arrow
        }
    });
    $('#return-to-top').click(function() {      // When arrow is clicked
        $('body,html').animate({
            scrollTop : 0                       // Scroll to top of body
        }, 500);
    });

    // $(window).click(function() {
    // //Hide the menus if visible
    //     $('#trigger').prop('checked', false);
    // });

    // $('nav-btn').click(function() {
    //     $('#trigger').prop('checked', true);
    // });

    
});